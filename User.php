<?php
    class User{
	
        private $nome,$idade,$genero,$descricao,$senha,$turma;
		
        function __construct($nome,$idade,$genero,$descricao,$senha,$turma){
			
			$this -> nome = $nome;
			$this -> idade = $idade;
			$this -> genero = $genero;
			$this -> descricao = $descricao;
			$this -> senha = $senha;
			$this -> turma = $turma;
		}
		
		function getNome(){
			return $this -> nome;
		}
		function getIdade(){
			return $this -> idade;
		}
		function getGenero(){
			return $this -> genero;
		}
		function getDescricao(){
			return $this -> descricao;
		}
		function getTurma(){
			return $this -> turma;
		}
		
		function setNome($nomeR){
			$nome = $nomeR;
			$this -> nome = $nome;
		}
		function setIdade($idadeR){
			$idade = $idadeR;
			$this -> idade = $idade;
		}
		function setGenero($generoR){
			$genero = $generoR;
			$this -> genero = $generoR;
		}
		function setDescricao($descricaoR){
			$descricao = $descricaoR;
			$this -> descricao = $descricao;
		}
		
		function setTurma($turmaR){
			$turma = $turmaR;
			$this -> turma = $turma;
		}
    }
?>